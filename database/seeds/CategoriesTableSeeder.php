<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Arts and Entertainment',
                'description' => ''
            ],
            [
                'name' => 'Cars & Other Vehicles',
                'description' => ''
            ],
            [
                'name' => 'Computers and Electronics',
                'description' => ''
            ],
            [
                'name' => 'Education and Communications',
                'description' => ''
            ],
            [
                'name' => ' Life',
                'description' => ''
            ],
            [
                'name' => 'Finance and Business',
                'description' => ''
            ],
            [
                'name' => 'Food Entertaining',
                'description' => ''
            ],
        ]);
    }
}
