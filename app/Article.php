<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const PUBLISHED = 1;
    const NOT_PUBLISHED = 0;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'category_id',
        'title',
        'representative_img',
        'content',
        'published'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('\App\Comment', 'article_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('\App\Category', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('\App\Tag', 'tag_articles');
    }

    /**
     * @param $tag_id
     */
    public function addTag($tag_id)
    {
        $this->tags()->attach($tag_id);
    }

    /**
     * @param $tag_id
     */
    public function removeTag($tag_id)
    {
        $this->tags()->detach($tag_id);
    }
}
