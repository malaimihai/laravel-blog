<?php

namespace App\Http\Controllers;

use App\Category;
use App\Article;
use App\Comment;
use App\Events\UserCommented;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    use UsersTrait;
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param int $articleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function write($articleId = 0)
    {
        Session::forget('_old_input');

        if(!empty($articleId) && $this->userHasArticle($articleId)) {
            $article = Article::find($articleId);
            session([
                '_old_input.article_id' => $article->id,
                '_old_input.category' => $article->category_id,
                '_old_input.title' => $article->title,
                '_old_input.text' => $article->content,
                '_old_input.tags' => implode($article->tags()->pluck('name')->all(), Tag::DELIMITER)
            ]);
        }

        return view('dashboard.pages.write', [
            'categories' => Category::all(),
            'editArticle' => isset($article) ? true : false
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createArticle(Request $request)
    {
        $data = $request->all();

        Validator::extend('checkCategory', function ($attribute, $value, $parameters, $validator) {
            return Category::where('id', $value)->first() != null;
        });

        $validator = Validator::make($data, [
            'title' => 'required',
            'category' => 'required|integer|checkCategory',
            'text' => 'required',
            'representative_img' => 'image'
        ], [
            'checkCategory' => 'Invalid category!'
        ]);

        if($validator->fails()) {
            return back()->withErrors($validator->messages())->withInput();
        }

        if($data['articleId'] && $this->userHasArticle($data['articleId'])) {
            $article = Article::find($data['articleId']);
        } else {
            $article = new Article();
        }

        $article->title = $data['title'];
        $article->user_id = Auth::user()->id;
        $article->category_id = $data['category'];
        $article->representative_img = isset($data['representative_img']) ? $data['representative_img']->getClientOriginalName() : null;
        $article->content = $data['text'];
        $article->save();

       Session::forget('_old_input');

        if($article->representative_img) {
            $img_path = "uploads/" . Auth::user()->id . "/articles/" . $article->id;

            if(!\File::exists($img_path)) {
                \File::makeDirectory($img_path, 0755, true, true);
            }

            $data['representative_img']->move($img_path,  $data['representative_img']->getClientOriginalName());
        }

        if(!empty($data['tags'])) {
            $tags = explode(Tag::DELIMITER, $data['tags']);
            if(count($tags) > 0) {
                foreach ($tags as $value) {
                    $tag = Tag::firstOrCreate(['name' => $value]);
                    $tag->name = $value;
                    $article->addTag($tag->id);
                }
            }
        }

        return redirect()->route('index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function articles()
    {
        // @TODO. get all articles when admin
        $articles = Auth::user()->articles()->get();

        return view('dashboard.pages.articles', [
            'articles' => $articles
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request)
    {
        $articleId = $request->get('articleId');

        if($this->userHasArticle($articleId)) {
            $article = Article::where('id', $articleId)->first();
            $article->published = $article->published == Article::PUBLISHED ? Article::NOT_PUBLISHED : Article::PUBLISHED;
            $article->save();

            return response()->json([
                'type' => 'success',
                'msg' => 'Successfully!'
            ]);
        } else {
            return response()->json([
                'type' => 'error',
                'msg' => 'Article does not exists!'
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createComment(Request $request)
    {
        if(!empty(trim($request->get('text')))) {
            $comment = new Comment();
            $comment->comment = $request->get('text');
            $comment->article_id = $request->get('article_id');
            $comment->user_id = Auth::user()->id;
            $comment->save();

            event(new UserCommented([
                'comment' => $comment,
                'user' => $comment->user()->first(),
            ]));
        }

        return response()->json([
            'type' => 'success',
            'msg' => 'Successfully!'
        ]);
    }
}
