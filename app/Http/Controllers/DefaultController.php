<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Illuminate\Support\Facades\Auth;

class DefaultController extends Controller
{
    use ArticlesTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::where('published', Article::PUBLISHED)->paginate(5);

        return view('pages.index', [
            'articles' => $articles,
            'randomArticles' => Article::inRandomOrder()->limit(5)->get(),
            'topViewedArticles' => $this->getTopViewedArticles()
        ]);
    }

    /**
     * @param $article_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article($article_id)
    {
        $article  = Article::where([
            ['id', '=', $article_id],
            ['published', '=', Article::PUBLISHED]
        ])->first();

        if(empty($article) && Auth::check()) {
            $article = Article::where([
                ['id', '=', $article_id]
            ])->first();
        }

        $comments = null;

        if(!empty($article)) {
            $comments = Article::find($article_id)->comments()->with('user')->orderBy('created_at', 'ASC')->get();
        }


        return view('pages.article', [
            'article' => $article,
            'user' => !empty($article) ? User::find($article->user_id) : '',
            'randomArticles' => Article::inRandomOrder()->limit(5)->get(),
            'topViewedArticles' => $this->getTopViewedArticles(),
            'comments' => $comments
        ]);
    }
}