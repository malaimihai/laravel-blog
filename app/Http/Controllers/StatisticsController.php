<?php

namespace App\Http\Controllers;

use App\Article;
use App\Statistic;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function add(Request $request)
    {
        $type = $request->get('type');
        $articleId = $request->get('articleId');

        if(!empty($type) &&
            !empty($articleId) &&
            in_array($type, [Statistic::CLICK_TYPE, Statistic::VIEW_TYPE])
        ) {
            if(Article::where('id', $articleId)->first() != null) {
                $statistic = new Statistic();
                $statistic->statistic_type = $type;
                $statistic->article_id = $articleId;
                $statistic->save();

                return response()->json([
                    'type' => 'success',
                    'msg' => 'Statistic added successfully! Type: ' . $type
                ]);
            }
        }

        return response()->json([
            'type' => 'error',
            'msg' => 'Failed to add statistic!'
        ]);
    }

}
