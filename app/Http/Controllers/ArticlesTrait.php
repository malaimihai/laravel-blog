<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 5/23/17
 * Time: 9:26 PM
 */

namespace App\Http\Controllers;


use App\Article;
use App\Statistic;
use Illuminate\Support\Facades\DB;

trait ArticlesTrait
{
    /**
     * @param int $limit
     * @return mixed
     */
    public function getTopViewedArticles($limit = 5)
    {
        $topViewedArticlesIds = Statistic::select('article_id', DB::raw('count(*) as total'))
            ->where('statistic_type', Statistic::VIEW_TYPE)
            ->groupBy('article_id')
            ->orderBy('total', 'DESC')
            ->limit($limit)
            ->pluck('article_id')
            ->toArray();

        $articles = Article::whereIn('id', $topViewedArticlesIds)->orderBy('created_at', 'ASC')->get();

        return $articles;
    }
}