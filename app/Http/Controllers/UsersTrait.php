<?php

namespace App\Http\Controllers;


use App\Article;
use App\User;
use Illuminate\Support\Facades\Auth;

trait UsersTrait
{
    /**
     * @param $articleId
     * @return bool
     */
    public function userHasArticle($articleId) {
        if(Auth::user()->role === User::ROLE_ADMIN) {
            return true;
        }

        $article = Article::where('id', $articleId)->first();

        return !empty($article) && Auth::user()->id === $article->user_id;
    }
}