<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    const VIEW_TYPE = 'view';
    const CLICK_TYPE = 'click';

    public $timestamps = false;
}
