<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    CONST DELIMITER = ',';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
