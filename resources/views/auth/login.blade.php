@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default" style="margin-top: 80px">
                <div class="panel-body">
                    <h3>Sign In</h3>
                    <p>Welcome to Coffeespot</p>
                    <br>
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="checkbox checkbox-primary">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="remember">
                                <label for="remember">Remember Me</label>
                                <a class="pull-right" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top: 40px">
                            <button type="submit" class="btn btn-primary btn-group-justified">
                                Sign In
                            </button>
                        </div>
                        <p style="margin-top: 30px">No account? <a href="{{ url('/register') }}">Sign Up</a></p>
                    </form>
                </div>
            </div>
    </div>
</div>
@endsection
