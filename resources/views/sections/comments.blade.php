<div class="row">
    <h3 class="title text-black">Comments ({{ !empty($comments) ? count($comments) : '0' }})</h3>
    @foreach($comments as $comment)
        <div class="col-xs-12 article-subsection">
            <img class="article-user-img" src="{{ asset("img/default_user_img.jpg") }}">
            <a href="#">{{ $comment->user()->first()->name }}</a>
            <span><i class="fa fa-calendar"></i> {{ $comment->created_at }}</span>
        </div>
        <div class="col-xs-12 article-subsection">
            <span style="margin-left: 15px">
                {{ $comment->comment }}
            </span>
        </div>
    @endforeach
    @if(\Illuminate\Support\Facades\Auth::check())
        <div class="col-xs-12" style="margin-top: 20px">
            <textarea id="commentText" class="form-control" placeholder="Write comment"></textarea><br>
            <button id="submitComment" class="btn btn-primary pull-right">Comment</button>
        </div>
    @endif
</div>

@section('javascript')
    <script>
        $(document).ready(function() {

            $('#submitComment').on('click', function() {
                var text = $('#commentText').val();
                $.ajax({
                    url: "{!! route('dashboard::create.comment') !!}",
                    type: "POST",
                    data: {
                        'text' : text,
                        'article_id': {!! json_encode($article->id) !!}
                    },
                    success: function (result) {
//                    console.log(result);
                    }
                });
            });
        });
    </script>
@endsection