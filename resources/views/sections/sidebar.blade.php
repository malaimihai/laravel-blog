<div class="row sidebar-section">
    <div class="col-xs-12">
        <h2 class="sidebar-widget-title">Social Media</h2>
    </div>
    <div class="col-xs-12">
        <a class="social-item social-facebook" style="margin-right: 10px">
            <span class="fa fa-facebook social-icon"></span>
            Facebook
        </a>
        <a class="social-item social-twitter">
            <span class="fa fa-twitter social-icon"></span>
            Twitter
        </a>
        <a class="social-item social-rss" style="margin-right: 10px">
            <span class="fa fa-user-plus social-icon"></span>
            Blogger
        </a>
        <a class="social-item social-google-plus">
            <span class="fa fa-google-plus social-icon"></span>
            Google +
        </a>
    </div>
    <div class="col-xs-12">
        <h2 class="sidebar-widget-title">Random Articles</h2>
    </div>
    @if(!empty($randomArticles))
        @foreach($randomArticles as $randomArticle)
            <div class="col-xs-12">
                <div class="sidebar-post">
                    @if(!empty($randomArticle->representative_img))
                        <a href="{{ url("/article/" . $randomArticle->id) }}">
                            <img src="{{ asset("/uploads/" . $randomArticle->user_id . "/articles/" . $randomArticle->id . '/' . $randomArticle->representative_img) }}">
                        </a>
                    @else
                        <a href="{{ url("/article/" . $randomArticle->id) }}">
                            <img src="{{ asset("/img/default_rep_img.jpg") }}">
                        </a>
                    @endif
                    <a href="{{ url("/article/" . $randomArticle->id) }}">{{ $randomArticle->title }}</a>
                </div>
            </div>
        @endforeach
    @else
        <div class="col-xs-12">
            <h3 class="title text-black">No articles!</h3>
        </div>
    @endif
    {{--<div class="col-xs-12">--}}
        {{--<h2 class="sidebar-widget-title">Kategori</h2>--}}
    {{--</div>--}}
    {{--<div class="col-xs-12">--}}
        {{--<select class="form-control select-category" style="margin-bottom: 25px">--}}
            {{--<option selected>Pilih Kategori</option>--}}
        {{--</select>--}}
    {{--</div>--}}
    <div class="col-xs-12">
        <h2 class="sidebar-widget-title">Most Viewed</h2>
    </div>
    @if(!empty($topViewedArticles) && count($topViewedArticles) > 0)
        @foreach($topViewedArticles as $topViewedArticle)
            <div class="col-xs-12">
                <div class="sidebar-post">
                    @if(!empty($topViewedArticle->representative_img))
                        <a href="{{ url("/article/" . $topViewedArticle->id) }}">
                            <img src="{{ asset("/uploads/" . $topViewedArticle->user_id . "/articles/" . $topViewedArticle->id . '/' . $topViewedArticle->representative_img) }}">
                        </a>
                    @else
                        <a href="{{ url("/article/" . $topViewedArticle->id) }}">
                            <img src="{{ asset("/img/default_rep_img.jpg") }}">
                        </a>
                    @endif
                    <a href="{{ url("/article/" . $topViewedArticle->id) }}">{{ $topViewedArticle->title }}</a>
                </div>
            </div>
        @endforeach
    @else
        <div class="col-xs-12">
            <h3 class="title text-black">No articles!</h3>
        </div>
    @endif
</div>
