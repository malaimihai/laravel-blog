<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="footer-wrapper">
                <div class="col-xs-7">
                    <ul class="footer-menu">
                        {{--<li><a href="#">SITEMAP</a></li>--}}
                        {{--<li><a href="#">CONTACT</a></li>--}}
                        {{--<li><a href="#">CSS MINIFIER</a></li>--}}
                        {{--<li><a href="#">HTML CONVERTER</a></li>--}}
                    </ul>
                </div>
                <div class="col-xs-5">
                    <ul class="footer-social-icons pull-right">
                        <li><a href="#"><span class="fa fa-google-plus social-google-plus"></span></a></li>
                        <li><a href="#"><span class="fa fa-facebook social-facebook"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter social-twitter"></span></a></li>
                        <li><a href="#"><span class="fa fa-rss social-rss"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row footer-copyright">
            <div class="footer-wrapper">
                <div class="col-xs-12">
                    <p class="text-center">
                        Copyright © 2017
                        <a href="#">Coffeespot </a>
                        All Right Reserved
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>