<nav class="navbar navbar-default">
    <div class="nav-wrapper">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('index') }}">Coffeespot</a>
            <p class="brand-text" style="display: none">Another Simplify 2 Responsive</p>
        </div>
        <div class="navbar-toogle-div">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="fa fa-th-list"></span>
                MENU
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            {{--<ul class="nav navbar-nav">--}}
                {{----}}
            {{--</ul>--}}
            <ul class="nav navbar-nav navbar-right">
                {{--<li>--}}
                    {{--<form class="navbar-form navbar-left navbar-input-group" role="search">--}}
                        {{--<div class="input-group">--}}
                            {{--<input type="text" class="form-control" placeholder="Cari artikel lainnya...">--}}
                            {{--<span class="input-group-btn">--}}
                                    {{--<button class="btn btn-default" type="button">--}}
                                        {{--<span class="fa fa-search"></span>--}}
                                    {{--</button>--}}
                                {{--</span>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</li>--}}
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Sign in</a></li>
                    <li><a href="{{ route('register') }}">Create Account</a></li>
                @else
                    <li><a href="{{ route('dashboard::write') }}">Write</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('dashboard::articles') }}">Articles</a> </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>