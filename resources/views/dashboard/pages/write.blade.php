@extends('layouts.app')

@section('content')
   <div class="row">
       <div class="col-md-12">
           <h3 class="title" style="margin-bottom: 20px">Write Article</h3>
       </div>
       <form method="post" action="{{ route('dashboard::create.article') }}" enctype="multipart/form-data">
           {{ csrf_field() }}
            <input type="hidden" value="{{ $editArticle ? old('article_id') : '' }}" name="articleId">
           <div class="form-group col-md-12 {{ $errors->has('category') ? ' has-error' : '' }}">
               <label class="control-label">
                   Category
                   <sup class="required_field">*</sup>
               </label>

               <select class="form-control" name="category">
                   <option value="0" disabled selected>Choose category</option>
                   @foreach($categories as $category)
                       <option
                               value="{{ $category->id }}"
                               {{ old('category') && old('category') == $category->id ? 'selected' : ''}}>
                           {{ $category->name }}</option>
                   @endforeach
               </select>

               @if ($errors->has('category'))
                   <span class="help-block">
                       <strong>{{ $errors->first('category') }}</strong>
                   </span>
               @endif
           </div>

           <div class="form-group col-md-12 {{ $errors->has('title') ? 'has-error' : '' }}">
               <label class="control-label">
                   Title
                   <sup class="required_field">*</sup>
               </label>

               <input type="text" class="form-control" value="{{ old('title') }}" placeholder="Title" name="title">
               <div class="description">
                   The title should be filled with meaning, so you can understand what the article will be about.
               </div>

               @if ($errors->has('title'))
                   <span class="help-block">
                       <strong>{{ $errors->first('title') }}</strong>
                   </span>
               @endif
           </div>

           <div class="form-group col-md-6">
               <label class="control-label">Representative image</label>
               <div class="input-group">
                   <span class="input-group-btn">
                       <span class="btn btn-default btn-file">
                           Browse… <input type="file" name="representative_img" id="imgInp">
                       </span>
                   </span>
                   <input type="text" class="form-control" readonly>
               </div>
               <img id='img-upload' }}/>
           </div>

           <div class="form-group col-md-12 {{ $errors->has('text') ? 'has-error' : '' }}">
               <label class="control-label">
                   Text
                   <sup class="required_field">*</sup>
               </label>

               <textarea id="editor-textarea" value="ddd" name="text" rows="10" cols="80"></textarea>

               @if ($errors->has('text'))
                   <span class="help-block">
                       <strong>{{ $errors->first('text') }}</strong>
                   </span>
               @endif
           </div>

           <div class="form-group col-md-12">
               <label class="control-label">Tags</label>

               <input name="tags" type="text" class="form-control" data-role="tagsinput" value="{{ old('tags') }}" name="tags">
               <div class="description">To add tag and hit enter(max 5 tags)</div>
           </div>

           <div class="form-group col-md-12" style="margin-bottom: 80px">
               <button type="submit" class="btn btn-default">Create Article</button>
           </div>
       </form>
   </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form").bind("keypress", function(e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
        });

        var oldText = {!! json_encode(old('text')) !!};

        CKEDITOR.replace('editor-textarea').setData(oldText);
    </script>
@endsection