@extends('layouts.app')

@section('content')
    @if(count($articles) == 0)
        <h3 class="text-black text-center" style="margin: 50px 0 380px 0"> No articles found! </h3>
    @else
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center text-black">All articles</h3>
                <table class="table text-black">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th class="text-right"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($articles as $article)
                            <tr id="article{{ $article->id }}" style="background-color: {{ $article->published == \App\Article::PUBLISHED ? "#97c997" : "#db8e8e"}}">
                                <td>{{ $article->id }}</td>
                                <td>{{ $article->title }}</td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ route('article', ['id' => $article->id]) }}"
                                                   target="_blank">Preview</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('dashboard::edit', ['id' => $article->id]) }}">Edit</a>
                                            </li>
                                            <li>
                                                <a href="#"
                                                   id="publishArticle{{ $article->id }}"
                                                   class="publishArticle"
                                                   data-toggle="modal"
                                                   data-target="#publishModal"
                                                   article-id="{{ $article->id }}">
                                                    {{ $article->published == \App\Article::PUBLISHED ? "Don't publish" : "Publish" }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-labelledby="publishModalLabel">
            <div class="modal-dialog modalMsg" role="document">
                <div class="modal-content">
                    <div class="modal-body text-right">
                        <div class="row">
                            <div class="col-md-9 text-center">
                                <h3 style="color: #ffffff">
                                <span class="">
                                </span>
                                    Change status of article: <span id="articleTitle"
                                                  style="text-decoration: underline; font-size: 20px"></span> ?
                                </h3>
                            </div>
                            <div class="col-md-3 text-center" style="margin-top: 35px">
                                <button type="button" class="btn btn-success articleRemoveConfirmation">Yes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('javascript')
    <script>
        var articleId;
        $(".publishArticle").on('click', function() {
            articleId = $(this).attr('article-id');
            $('#articleTitle').text("'" + $("tr#article" + articleId + " td:nth-child(2)").text() + "'");
        });

        $(".articleRemoveConfirmation").on('click', function() {
            $.ajax({
                url: "{!! route('dashboard::article.publish') !!}",
                type: "POST",
                data: {
                    'articleId' : articleId
                },
                success: function (result) {
                    if (result.type == "success") {
                        $("#publishModal").modal('toggle');

                        var articleTr = $("#article" + articleId);

                        if(articleTr.css('backgroundColor') == "rgb(219, 142, 142)") {
                            articleTr.css('background-color', '#97c997');
                            $("#publishArticle" + articleId).text("Don't publish");

                        } else {
                            articleTr.css('background-color', '#db8e8e');
                            $("#publishArticle" + articleId).text("Publish");
                        }
                    }
                }
            });
        });
    </script>
@endsection