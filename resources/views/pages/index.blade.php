@extends('layouts.app')

@section('content')
    <div class="row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.05);">
        <div class="col-sm-8">
            <div class="row section-main">
                @if(count($articles) == 0)
                    <h3 class="text-black text-center" style="margin-top: 50px"> No articles found! </h3>
                @else
                    @foreach($articles as $article)
                        <div class="post">
                            <div class="col-xs-5">
                                @if(!empty($article->representative_img))
                                    <a href="{{ url('article/' . $article->id) }}"> <img src="{{ asset( "/uploads/" . $article->user_id . "/articles/" . $article->id . '/' . $article->representative_img) }}" class="img-responsive"></a>
                                @else
                                    <a href="{{ url('article/' . $article->id) }}"> <img src="{{ asset('img/default_rep_img.jpg') }}"></a>
                                @endif
                            </div>
                            <div class="col-xs-7">
                                <h2 class="post-title"><a href="{{ url('article/' . $article->id) }}">{{ $article->title }}</a></h2>
                                <div class="post-info">
                                    <span><i class="fa fa-user"></i><a href="#">{{ \App\User::find($article->user_id)->name }}</a></span>
                                    <span><i class="fa fa-calendar-o"></i><a href="#">{{ \Carbon\Carbon::parse($article->created_at)->format('Y-m-d') }}</a></span>
                                    <span><i class="fa fa-comments-o"></i><a href="#"></a></span>
                                </div>
                                <div class="articles-content">
                                    {!!  $article->content !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-xs-12 post" style="border-bottom: none">
                        {{ $articles->links() }}
                    </div>
                @endif
            </div>
        </div>
        <div class="col-sm-4">
            @include('sections.sidebar')
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $(".articles-content").each(function() {
                var articleText =$(this).text();
                $(this).html(articleText.trim().substring(0, 100).trim() + '...');
            });


        });
    </script>
@endsection