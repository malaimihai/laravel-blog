@extends('layouts.app')

@section('content')
    <div class="row">
        @if($article == null)
            <div class="col-md-12">
                <h3 class="text-black text-center" style="margin-top: 50px"> Article was not found! </h3>
            </div>
        @else
            <div class="col-sm-8">
                <div class="row section-main">
                    @if($article->published == \App\Article::NOT_PUBLISHED)
                        <div class="alert alert-danger" role="alert">
                            <span class="fa fa-exclamation-circle" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Article is not published!
                        </div>
                    @endif
                    <div class="col-md-12 article-subsection">
                        <h3 class="title">{{ $article->title }}</h3>
                        <small>{{ $article->category()->first()->name }}</small>
                    </div>
                    <div class="col-md-12 article-subsection">
                        <img class="article-user-img" src="{{ asset("img/default_user_img.jpg") }}">
                        <a href="#">{{ $user->name }}</a>
                        <span><i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($article->created_at)->format('Y-m-d') }}</span>
                    </div>
                    <div class="col-md-12 article-subsection">
                        <span class="label label-default">#Tags</span>
                        @foreach($article->tags()->get() as $tag)
                            <span class="label label-primary">#{{ $tag->name }}</span> &nbsp;
                        @endforeach
                    </div>
                    <div class="col-md-12 article-subsection text-black addClickStatistic">
                        @if(!empty($article->representative_img))
                            <img class="single-article-img" src="{{ asset( "/uploads/" . $article->user_id . "/articles/" . $article->id . '/' . $article->representative_img) }}">
                        @endif
                        <div class="text-justify">
                            {!! $article->content  !!}
                        </div>
                    </div>
                    <div class="col-md-12 article-subsection">
                        <div id="app">
                            <comments :comments="{{ $comments }}" :auth="{{ \Illuminate\Support\Facades\Auth::check() ? 1 : 0 }}" :articleid="{{ $article->id }}"></comments>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                @include('sections.sidebar')
            </div>
        @endif
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            var article = {!! json_encode($article) !!}
            addStatistics('{{ route('statistics::add') }}', 'view', article.id);

            $(document).on('click', '.addClickStatistic', function() {
                addStatistics('{{ route('statistics::add') }}', 'click', article.id);
            });
        });
    </script>
@endsection