<?php


Auth::routes();

/**
 * LandingPage Routes
 */
Route::get('/', 'DefaultController@index')->name('index');
Route::get('/article/{id}', 'DefaultController@article')->name('article');

/**
 * Dashboard Routes
 */
Route::group(['as' => 'dashboard::'], function() {
    Route::get('/dashboard/write', 'DashboardController@write')->name('write');
    Route::get('/dashboard/edit/{id}', 'DashboardController@write')->name('edit');
    Route::get('/dashboard/articles', 'DashboardController@articles')->name('articles');

    Route::post('/dashboard/create/article', 'DashboardController@createArticle')->name('create.article');
    Route::post('/dashboard/article/publish', 'DashboardController@publish')->name('article.publish');
    Route::post('/dashboard/create/comment', 'DashboardController@createComment')->name('create.comment');
});

/**
 * Statistics Routes
 */
Route::group(['as' => 'statistics::'], function() {
    Route::post('/statistics/add', 'StatisticsController@add')->name('add');
});
